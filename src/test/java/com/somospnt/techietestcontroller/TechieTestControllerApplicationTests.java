package com.somospnt.techietestcontroller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.util.NestedServletException;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class TechieTestControllerApplicationTests {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void entrarABarra_devuelveViewHome() throws Exception {
        mockMvc
                .perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(view().name("home"));
    }

    @Test
    public void entrarApaginaInexistente_devuelveStatus404() throws Exception {

    }

    /*
        Testear que:
            1) Que devuelva status 200 (ok)
            2) Nos envíe a la view personas
            3) Que el modelo tenga el atributo "personas" y que este tenga tamaño 2
     */
    @Test
    public void entrarABarraPersonas_con3PersonasExistentes_devuelveLaVistaPersonaConLosDatosDeLas3Personas() throws Exception {

    }

    /*
        La view personasConError llama a un atributo de persona que no existe.
        Testear que lance NestedServletException
     */
    @Test(expected = NestedServletException.class)
    public void entrarABarraPersonasConError_con3PersonasExistentes_lanzaNestedServletException() throws Exception {

    }

    /*
        Testear que:
            1) Que devuelva status 302 (found)
            2) Nos redirija a la url personas
     */
    @Test
    public void entrarARedirigimeAPersonas_redirigeALaViewPersonas() throws Exception {

    }

    /*
        Guarda que este es un servicio rest!!
        Testear que:
            1) Que devuelva status 200 (found)
            2) Nos devuelva un json (APPLICATION_JSON_UTF8)
            3) Que la persona devuelta tenga el nombre que buscamos (ver json path)

            No hardcodear el param en la url ;)
     */
    @Test
    public void buscarPersonaPorNombre_conNombreExistente_devuelveLaPersonaCorrespondiente() throws Exception {

    }

    /*
        Guarda que este es un servicio rest!!
        Testear que:
            1) Que devuelva status 404 (not found)
     */
    @Test
    public void buscarPersonaPorNombre_conNombreInexistente_devuelveStatus404() throws Exception {

    }
}