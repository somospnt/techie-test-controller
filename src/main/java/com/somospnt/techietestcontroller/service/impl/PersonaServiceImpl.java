package com.somospnt.techietestcontroller.service.impl;

import com.somospnt.techietestcontroller.domain.Persona;
import com.somospnt.techietestcontroller.service.PersonaService;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

@Service
public class PersonaServiceImpl implements PersonaService {

    private static final List<Persona> personas = Arrays.asList(
            new Persona("Aquiles", 25),
            new Persona("Va", 30),
            new Persona("Esa", 35)
    );

    @Override
    public List<Persona> buscarTodas() {
        return personas;
    }

    @Override
    public Persona buscarPorNombre(String nombre) {
        return personas
                .stream()
                .filter(persona -> persona.getNombre().equals(nombre))
                .findFirst()
                .orElseThrow(NoSuchElementException::new);
    }
}
