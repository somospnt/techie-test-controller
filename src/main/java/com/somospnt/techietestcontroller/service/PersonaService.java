package com.somospnt.techietestcontroller.service;

import com.somospnt.techietestcontroller.domain.Persona;

import java.util.List;

public interface PersonaService {

    List<Persona> buscarTodas();

    Persona buscarPorNombre(String nombre);
}