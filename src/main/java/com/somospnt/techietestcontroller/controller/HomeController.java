package com.somospnt.techietestcontroller.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

    @GetMapping("/")
    public String home() {
        return "home";
    }

    @GetMapping("/redirigime-a-personas")
    public String redirigimeAPersonas() {
        return "redirect:personas";
    }
}
