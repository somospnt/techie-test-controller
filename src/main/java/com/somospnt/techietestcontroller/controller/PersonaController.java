package com.somospnt.techietestcontroller.controller;

import com.somospnt.techietestcontroller.domain.Persona;
import com.somospnt.techietestcontroller.service.PersonaService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class PersonaController {

    private final PersonaService personaService;

    public PersonaController(PersonaService personaService) {
        this.personaService = personaService;
    }

    @GetMapping("/personas")
    public String buscarTodas(Model model) {
        List<Persona> personas = personaService.buscarTodas();
        model.addAttribute("personas", personas);

        return "personas";
    }

    @GetMapping("/personas-con-error")
    public String personasConError(Model model) {
        List<Persona> personas = personaService.buscarTodas();
        model.addAttribute("personas", personas);

        return "personasConError";
    }

}