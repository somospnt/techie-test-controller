package com.somospnt.techietestcontroller.controller.rest;

import com.somospnt.techietestcontroller.domain.Persona;
import com.somospnt.techietestcontroller.service.PersonaService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PersonaRestController {

    private final PersonaService personaService;

    public PersonaRestController(PersonaService personaService) {
        this.personaService = personaService;
    }

    @GetMapping(value = "/personas", params = "nombre")
    public Persona bsucarPorNombre(@RequestParam String nombre) {
        return personaService.buscarPorNombre(nombre);
    }
}
