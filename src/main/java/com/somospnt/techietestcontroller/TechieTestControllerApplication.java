package com.somospnt.techietestcontroller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TechieTestControllerApplication {

    public static void main(String[] args) {
        SpringApplication.run(TechieTestControllerApplication.class, args);
    }
}
